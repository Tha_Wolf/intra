<?php

use Illuminate\Http\Request;


/*Route::get("/DirectorioTelefonico","DirectorioTelefonicoController@index");


Route::get("/sessiondata", "SessionDataController@index");*/

Route::group([
  'middleware'=> 'api',
],
function(){
Route::post('login','AuthController@login');
Route::get("/DirectorioTelefonico","DirectorioTelefonicoController@index");
Route::get("/posts","PostController@index");
Route::get("/apps","AppController@index");
Route::post('/like','LikeController@create');
Route::post('/likecount','LikeController@count');
Route::post('likehabilitado','LikeController@likehabilitado');
Route::get('/downloadNormalizacion','DownloadController@downloadNormalizacion');

});
