<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = Like::all();

      return $data;
    }

    public function likehabilitado()
    {

      $data = request(['username_id']);
      $username = $data['username_id'];
      $resultado = Like::where('username_id','=',$username)
      ->get();
      return ($resultado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $data = request(['username_id', 'post_id']);

     //$post_id = $data->'username_id';

  $like = new Like;
  $like->username_id = $data['username_id'];
  $like->post_id = $data['post_id'];
  $like->save();
  return $like->username_id;
  Session::flash('message', "Te gusta!");

    }

    public function count(Request $request){

      $data = request(['post_id']);

      $likes = DB::table('likes')->where('post_id', $data['post_id'])->count();
      return json_encode($likes);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function show(Like $like)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function edit(Like $like)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Like $like)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like)
    {
        //
    }
}
