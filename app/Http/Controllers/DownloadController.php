<?php

namespace App\Http\Controllers;
use Adldap\Laravel\Facades\Adldap;

use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      /
    }

    public function downloadNormalizacion($link_path)
    {
        $root_path= public_path(). "/public/doc-normalizacion/";
        $file_path= $root_path.$link_path;
        return response()->download($file_path, 'filename.pdf');


    }
}
